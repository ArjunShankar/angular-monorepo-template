Steps
1) ng g library monorepo-shared
2) ng g application app-one
3) ng g application app-two 

Note: Next time use "--createApplication=true|false" when doing "ng new"  .. this will not create the default blank application.

Building
1) ng build monorepo-shared  (is a prod build by default)  ??  build required after every update? - 
2) ng serve --project=app-one


Links
https://blog.angularindepth.com/angular-workspace-no-application-for-you-4b451afcc2ba
https://blog.angularindepth.com/creating-a-library-in-angular-6-87799552e7e5
https://blog.angularindepth.com/creating-a-library-in-angular-6-part-2-6e2bc1e14121
https://blog.angularindepth.com/the-angular-library-series-publishing-ce24bb673275



TODOs
1) models, modules, components etc in the shared lib - use primeng as example
2) extending components in applications etc
3) just publishing the library on npm
