/*
 * Public API Surface of monorepo-shared
 */

export * from './lib/monorepo-shared.service';
export * from './lib/monorepo-shared.component';
export * from './lib/monorepo-shared.module';
