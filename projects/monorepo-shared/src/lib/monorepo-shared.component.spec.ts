import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonorepoSharedComponent } from './monorepo-shared.component';

describe('MonorepoSharedComponent', () => {
  let component: MonorepoSharedComponent;
  let fixture: ComponentFixture<MonorepoSharedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonorepoSharedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonorepoSharedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
