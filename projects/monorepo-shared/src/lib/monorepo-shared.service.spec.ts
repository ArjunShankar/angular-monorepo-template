import { TestBed } from '@angular/core/testing';

import { MonorepoSharedService } from './monorepo-shared.service';

describe('MonorepoSharedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MonorepoSharedService = TestBed.get(MonorepoSharedService);
    expect(service).toBeTruthy();
  });
});
