import { NgModule } from '@angular/core';
import { MonorepoSharedComponent } from './monorepo-shared.component';

@NgModule({
  declarations: [MonorepoSharedComponent],
  imports: [
  ],
  exports: [MonorepoSharedComponent]
})
export class MonorepoSharedModule { }
