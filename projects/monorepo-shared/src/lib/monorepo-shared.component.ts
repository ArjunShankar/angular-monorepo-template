import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-component-monorepo-shared',
  template: `
    <p>
      component from the monorepo-shared works!
    </p>
  `,
  styles: []
})
export class MonorepoSharedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
