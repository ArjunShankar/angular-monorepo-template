import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MonorepoSharedModule } from 'monorepo-shared';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MonorepoSharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
